import { B64EncodedString } from "@legoya-website/types";

export interface IOeuvreViewModel {
    title: string
    description: string
    created_at: string
    tags: string[]
    src: B64EncodedString
    metadatas: any
}

export interface IOeuvreGroupeViewModel {
    groupLabel: string;
    oeuvreList: Array<IOeuvreViewModel>;
}