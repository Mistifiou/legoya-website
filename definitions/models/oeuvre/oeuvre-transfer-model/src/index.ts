import { B64EncodedString, UUID } from "@legoya-website/types";

export interface IOeuvreTransferModel {
    id: UUID
    title: string
    description: string
    created_at: string
    tags: string[]
    src: B64EncodedString
    metadatas: any
}

export interface IOeuvreGroupeTransferModel {
    groupLabel: string;
    oeuvreList: Array<UUID>;
}