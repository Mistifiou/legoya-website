import { UUID } from "@legoya-website/types";



export default interface IOeuvreBusinessModel {
    id: UUID
    title: string
    description: string
    created_at: Date
    tags: string[]
    src: Blob
    metadatas: any
}