import { UUID } from "@legoya-website/types";

export interface IOeuvreDataModel {
    // -- id technique unique --
    id: UUID
    // -- id fonctionnel unique --
    title: string
    // ----
    description: string
    created_at: Date
    tags: string[]
    src: Blob
    metadatas: any
}