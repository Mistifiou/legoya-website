export default [
    {
        title: "Legoya et Alaya",
        description: "Bien le bonjour ! Je m’excuse de ne pas pu avoir poster  en juillet mais j’ai eu quelques  soucis d’orage on va dire. Oui quand la foudre tombe sur votre maison ça fait des dégâts mais bien heureusement pour nous que matériels ! Donc me voici aujourd’hui accompagnée  de cette illustration  pour marquer les mois de juillet et  d’Août. Je vous présente donc Legoya à gauche et alaya à droite ! Elles sont toutes les deux mes tous premiers  OC . Elles ont bien plus de 10 ans d’existence aujourd’hui. Je les ai bien dépoussiérées. Je vous raconterais  bientôt leurs histoires sur fond de Babylone.  Ce que vous pouvez savoir sur elles c’est que sur ce dessin elles ont le même  âge  à deux époques  différentes  et qu’elles sont étroitement liées l’une à l’autre. Je suis plutôt fière du résultat de cette illustration. Et vous, qu’en pensez vous ?",
        tags: [ "comics", "pinup", "illustration", "evenements", "devils", "monster", "alien", "manga" ],
    },
]