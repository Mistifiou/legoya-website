
import Violetta from './violetta';
import Legoya_Alaya from './legoya_alaya';
import MadSession from './mad-session_2019';
import Pact from './pacte';
import Pinup from './pinup';
import Ukyo from './ukyo';

export default [
    Violetta,
    Legoya_Alaya,
    MadSession,
    Pact,
    Pinup,
    Ukyo
]