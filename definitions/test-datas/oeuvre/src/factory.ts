
import * as deepmerge from 'deepmerge'
// @ts-ignore
import dayjs from 'dayjs'
import { IOeuvreTransferModel, IOeuvreGroupeTransferModel } from '@legoya-website/oeuvre-tm'
import { IOeuvreDataModel } from '@legoya-website/oeuvre-dm'
import { UUID } from '@legoya-website/types'
import FileMock from '../images/files'
import DataMock from '../images/datas'
import { LoremIpsum } from 'lorem-ipsum'
import 'dayjs/locale/fr';
import uuid from 'uuid/v4';
import atob from 'atob';
// const uuid = require('uuid/v4');
// const atob = require('atob');

dayjs.locale('fr')

export interface IGenerateGroupsOptions {
    groupCount?: number;
    perGroup?: IGroupOptions;
}

export interface IGroupOptions {
    minOeuvres?: number;
    maxOeuvres?: number;
}
export interface IListOptions {
    minOeuvres?: number;
    maxOeuvres?: number;
}

export interface IFileMockData {
    metadatas: any
    src: string
}
export interface IDataMockData {
    description: string
    title: string
    tags: string[]
}

const DEFAULT_GENERATE_GROUP_OPTS: IGenerateGroupsOptions = {
    groupCount: 12,
    perGroup: {
        minOeuvres: 12,
        maxOeuvres: 6*12
    }
}

export class ImageOeuvreFactory {

    private _ipsum = new LoremIpsum({
        sentencesPerParagraph: {
          max: 4,
          min: 2
        },
        wordsPerSentence: {
          max: 8,
          min: 4
        }
      });
    private _previousDate: any;
    
    generateGroupViewDataList (options: IGenerateGroupsOptions = {}) {
        options = deepmerge(DEFAULT_GENERATE_GROUP_OPTS, options);
        const groups: Array<IOeuvreGroupeTransferModel> = [];

        this._previousDate = dayjs();

        for (let groupIndex = 0; groupIndex < options.groupCount; groupIndex++) {
            groups.push(this.generateRandomGroup(options.perGroup));
        }
        return groups;
    }

    generateRandomGroup (options: IGroupOptions): IOeuvreGroupeTransferModel {
        const groupe: IOeuvreGroupeTransferModel = {
            groupLabel: this._getNextDate(),
            oeuvreList: this.generateRandomUUIDList(options)
            // oeuvreList: this.generateRandomOeuvreList(options)
        }

        return groupe
    }

    generateRandomOeuvreList (idList: Array<UUID>): Array<IOeuvreTransferModel> {
        const result: Array<IOeuvreTransferModel> = [];
        const tmpIdList: Array<UUID> = [...idList];

        for (let i = 0; i < idList.length; i++) {
            const customFile: IFileMockData = this._getRandomFileMockData();
            const viewModel: IOeuvreTransferModel = {
                id: tmpIdList.pop(),
                created_at: this._generateRandomDateBasedOnPrevious(),
                description: this._generateDescription(),
                src: customFile.src,
                metadatas: customFile.metadatas,
                tags: this._generateTags(),
                title: this._generateTitle(),
            }
            result.push(viewModel);                    
        }

        return result;
    }
    
    generateRandomOeuvreTransferModelList (idList: Array<UUID>): Array<IOeuvreTransferModel> {
        const result: Array<IOeuvreTransferModel> = [];
        const tmpIdList: Array<UUID> = [...idList];

        for (let i = 0; i < idList.length; i++) {
            const customFile: IFileMockData = this._getRandomFileMockData();
            const viewModel: IOeuvreTransferModel = {
                id: tmpIdList.pop(),
                created_at: dayjs().format('YYYY-MM-DD'),
                description: this._generateDescription(),
                src: customFile.src,
                metadatas: { format: 'image/jpeg' },
                tags: this._generateTags(),
                title: this._generateTitle(),
            }
            result.push(viewModel);                    
        }

        return result;
    }

    generateRandomOeuvreModelList (idList: Array<UUID>): Array<IOeuvreDataModel> {
        const result: Array<IOeuvreDataModel> = [];
        const tmpIdList: Array<UUID> = [...idList];

        for (let i = 0; i < idList.length; i++) {
            const customFile: IFileMockData = this._getRandomFileMockData();
            const viewModel: IOeuvreDataModel = {
                id: tmpIdList.pop(),
                created_at: new Date(),
                description: this._ipsum.generateParagraphs(1),
                src: atob (customFile.src) as any,
                metadatas: customFile.metadatas,
                tags: this._generateTags(),
                title: this._ipsum.generateWords(1),
            }
            result.push(viewModel);                    
        }

        return result;
    }

    generateRandomUUIDList (options: IListOptions): Array<UUID> {
        const count = Math.floor(Math.random() * (+options.maxOeuvres - +options.minOeuvres)) + +options.minOeuvres;
        const result: Array<UUID> = [];

        for (let i = 0; i < count; i++) {
            result.push(uuid());                    
        }

        return result;
    }
    /**
     * @todo: options
     */
    private _generateTitle() {
        const random = Math.floor(Math.random() * (+4 - +1)) + +1;
        return this._ipsum.generateWords(random)
    }

    /**
     * @todo: options
     */
    private _generateTags() {
        const random = Math.floor(Math.random() * (+10 - +6)) + +6;
        const result = [];
        for (let i = 0; i < random; i++) {
            result.push(this._ipsum.generateWords(1))        
        }
        return result
    }

    /**
     * @todo: options
     */
    private _generateDescription() {
        const random = Math.floor(Math.random() * (+6 - +1)) + +1;
        return this._ipsum.generateParagraphs(random)
    }

    /** @deprecated */
    private _getRandonDataMockData (): IDataMockData {
        const max = DataMock.length - 1;
        const random = Math.floor(Math.random() * (+max - +0)) + +0;
        return DataMock[random];
    }

    private _getRandomFileMockData (): IFileMockData {
        const max = FileMock.length;
        const random = Math.floor(Math.random() * (+max - +0)) + +0;
        return FileMock[random];
    }

    private _generateRandomDateBasedOnPrevious (): string {
        const startDate = this._previousDate || dayjs();
        
        const random = Math.floor(Math.random() * (+20 - +0)) + +0;
        const date = startDate.add(random, 'day');

        return date.format('DD MMMM YYYY')
    }

    private _getNextDate (): string {
        const startDate = this._previousDate || dayjs();
        
        const date = startDate.add(-1, 'year');
        
        this._previousDate = date;

        return date.format('YYYY')
    }
}