# Legoya's website

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)
[![pipeline status](https://gitlab.com/Mistifiou/legoya-website/badges/dev/pipeline.svg)](https://gitlab.com/user/userproject/commits/master)
[![pipeline status](https://gitlab.com/Mistifiou/legoya-website/badges/dev/pipeline.svg)](https://gitlab.com/Mistifiou/legoya-website/commits/master)
![node](https://img.shields.io/node/v/latest)
![angular](https://img.shields.io/badge/angular-8-blue)
![angular](https://img.shields.io/badge/caprover-8-blue)

## A propos

L'objectif premier est de Créer un site onePage afin de permettre la consultation des oeuvres.  

## Client

Client Angular 8, librairie graphique maison.  
Pour plus d'information sur le sujet: [mich-ui]()

La page contiens principalement:
- Une page pour lister les oeuvres et les regrouper par année.   
- Des éléments de navigation vers les réseaux sociaux.  

Chaque carte est consultable et visualisable en plein écran.  
Le client communique avec le service des oeuvres en JSON/REST.  

## Serveur

Un service "Oeuvres" joue les rôles d'authorisation et de gestion de la ressource "oeuvre".  
Connecté à une base de donnée mongo-db.  

## CI

Déploiements gérés avec Caprover au push sur les branches dev, release et master.  

### Feature

Avant qu'une branche soit mergée sur dev, elle est testée.

**todo:** Tester de faire une version consultable directement en ligne de la dev.

### Developpement

**todo:** La branche dev est consultable sur dev.legoya.fr (version dev)

### Release

**todo:** Une réplique de la production est disponible pendant les releases

### Production

La production représente le site vu par les visiteurs.  

## Architecture

Projet "monorepo" contenant le client, les services et les dépendances communes.  
Organisé via Lerna et un ensemble de scripts.  

### definitions

Package particulier contenant les types et les données de test.  

## Dépendances

[DayJS](https://github.com/iamkun/dayjs)
[deepmerge](https://github.com/TehShrike/deepmerge)