#!/bin/bash

# https://stackoverflow.com/questions/2220301/how-to-evaluate-http-response-codes-from-bash-shell-script

echo "Listening to website"
status_200=0
count=0

while [[ $status_200 != 1 ]]; do
    timeout 3
    status_code=$(curl -u legoya:PasswordBidon --silent --write-out %{http_code} --output /dev/null https://legoya-dev-client.sys.legoya.fr/)

    if [[ "$status_code" -ne 200 ]] ; then
        echo Current status $status_code
        count=$((count+1))
    else
        echo "Status 200 ok"
        status_200=1
    fi

    if [[ count -gt 200 ]] ; then
        echo ERROR: Timeout after $count attemps
        exit 1
    fi
done
