#!/bin/bash

# ##################################
# En bash car source n'est pas dispo en sh
# https://askubuntu.com/questions/504546/error-message-source-not-found-when-running-a-script
# ##################################

echo "fix permission error"
echo "unsafe-perm=true" >> ~/.npmrc

echo "Change directory for global npm install"
npm config set prefix $CI_PROJECT_DIR/node_modules --global

echo "update path to call global node modules like usual"
echo "export PATH=$CI_PROJECT_DIR/node_modules/bin:$PATH" >> ~/.profile
source ~/.profile