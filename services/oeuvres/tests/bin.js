const app = require('../src/bin');
const assert = require('assert');
const bcrypt = require('bcrypt');

/**
 * On commence par lance le contexte applicatif
 */
describe('Application context', function () {
    this.timeout(10000); 
    before(() => {
        return (app.ready === true) || app.ready.then(() => {
            /** Il faut un user "admin" par défaut */
            return app.modules.MongoRepositoryModule.models.Account.insertMany([{
                password: bcrypt.hashSync("legoya", 10),
                login: "legoya",
                role: "admin"
            }])
        })
    })
    after(() => {
        app.stop()
        return app.modules.MongoRepositoryModule.models.Account.deleteMany({
            login: "legoya"
        })
    })

    it('Should start', () => assert(true))

    require('./security/authenticate.test')
    require('./security/refreshToken.test')
    require('./oeuvre/getOeuvreGroupList.test')
    require('./oeuvre/getOeuvre.test')
    require('./oeuvre/addOeuvre.test')
    require('./oeuvre/findOeuvre.test')
})