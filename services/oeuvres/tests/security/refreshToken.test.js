const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const expect = chai.expect;
const { ImageOeuvreFactory } = require('@legoya-website/oeuvre-test-datas');
const app = require('../../src/bin');
const { authenticateAdmin } = require('../tools/requests');

chai.use(chaiHttp);

function refreshTokenRequest(_token, tests) {
    const path = `/api/v1/token`;
    console.log(`sending request to ${path}`)
    console.log(`Authorization: Bearer ${_token}`)
    return chai.request('127.0.0.1:7009')
        .put(path)
        .set('Authorization', `Bearer ${_token}`)
        .set('content-type', 'application/json')
        .then((res) => {
            console.log(`recieve result for ${path}`)
            return tests(res)
        }, (err) => {
            console.log(`recieve error for ${path}`)
            assert(false, err.stack);
        });
}

let token = '';

describe('refresh token tests', function () {
    this.timeout(5000); 
    before(() => {
        return authenticateAdmin().then((result) => {
            token = JSON.parse(result.text).token;
        })
     })
    after(() => { })

    it('return http status 200 if refresh success', () => {
        return refreshTokenRequest(token, (res) => {
            expect(res, res.text).to.have.status(200)
            console.log('Response recieved', res);
            const resultContent = JSON.parse(res.text);
            console.log(resultContent.token)
        })
    })
    it('return http status 401 with bad token', () => {
        return refreshTokenRequest('Bearer toto', (res) => {
            console.log('Response recieved');
            expect(res, res.text).to.have.status(401)
        })
    })
})