const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const expect = chai.expect;
const { ImageOeuvreFactory } = require('@legoya-website/oeuvre-test-datas');
const app = require('../../src/bin');
const bcrypt = require('bcrypt');


chai.use(chaiHttp);

function authenticateRequest(login, password, tests) {
    const path = `/api/v1/authenticate`;
    console.log(`sending request to ${path}`)
    return chai.request('127.0.0.1:7009')
        .post(path)
        .set('content-type', 'application/json')
        .send({ login, password })
        .then((res) => {
            console.log(`recieve result for ${path}`)
            return tests(res)
        }, (err) => {
            console.log(`recieve error for ${path}`)
            assert(false, err.stack);
        });
}

describe('authenticate tests', function () {
    this.timeout(5000); 
    before(() => {
        return app.modules.MongoRepositoryModule.models.Account.insertMany([{
            password: bcrypt.hashSync("auth-test", 10),
            login: "auth-test",
            role: "admin"
        }])
    })
    after(() => {
        console.log('Deleting test datas')
        return app.modules.MongoRepositoryModule.models.Account.deleteMany({
            login: "auth-test"
        });
    })
    it('return http status 201 if auth success', () => {
        return authenticateRequest("auth-test", "auth-test", (res) => {
            console.log('Response recieved');
            expect(res, res.text).to.have.status(201)
            expect(res).to.be.json;
            expect(JSON.parse(res.text)).to.have.property("token");
        })
    })
    it('return http status 404 with bad password', () => {
        return authenticateRequest("auth-test", "kukrapoc", (res) => {
            console.log('Response recieved');
            expect(res, res.text).to.have.status(404)
        })
    })
    it('return http status 404 with unknown login/password', () => {
        return authenticateRequest("kukrapoc", 'kukrapoc', (res) => {
            console.log('Response recieved');
            expect(res, res.text).to.have.status(404)
        })
    })
})