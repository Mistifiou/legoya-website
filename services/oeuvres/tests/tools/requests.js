
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

function authenticateRequest(login, password) {
    const path = `/api/v1/authenticate`;
    console.log(`sending request to ${path}`)
    return chai.request('127.0.0.1:7009')
        .post(path)
        .set('content-type', 'application/json')
        .send({ login, password });
}
function authenticateAdmin() {
    return authenticateRequest('legoya', 'legoya');
}
function authenticateAnonymous() {
    return authenticateRequest('anonymous', 'anonymous');
}

module.exports = {
    authenticateAdmin,
    authenticateRequest,
    authenticateAnonymous
}