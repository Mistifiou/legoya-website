const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const expect = chai.expect;
const { ImageOeuvreFactory } = require('@legoya-website/oeuvre-test-datas');
const app = require('../../src/bin');
const { authenticateAdmin } = require('../tools/requests');

chai.use(chaiHttp);

function getOeuvreRequest(id, tests) {
    const path = `/api/v1/oeuvre/${id}`;
    console.log(`sending request to ${path}`)
    return chai.request('127.0.0.1:7009')
        .get(path)
        .set('Authorization', `Bearer ${token}`)
        .then((res) => {
            console.log(`recieve result for ${path}`)
            return tests(res)
        }, (err) => {
            console.log(`recieve error for ${path}`)
            assert(false, err.stack);
        });
}

let token = '';

describe('getOeuvre tests', function () {
    this.timeout(5000); 
    let knownId, unknownId;
    before(() => {
        const factory = new ImageOeuvreFactory();
        const oeuvreList = factory.generateRandomOeuvreModelList(factory.generateRandomUUIDList({ minOeuvres: 100, maxOeuvres: 100 }));
        return app.modules.MongoRepositoryModule.models.Oeuvre.insertMany(oeuvreList).then((results) => {
            knownId = results[0]._id;
            unknownId = 'aaaaaaaaaaaaaaaaaaaaaaaa';
            return authenticateAdmin().then((result) => {
                token = JSON.parse(result.text).token;
            })
        })
    })
    after(() => {
        console.log('Deleting test datas')
        return app.modules.MongoRepositoryModule.models.Oeuvre.deleteMany();
    })
    it('return http status 200 with known id', () => {
        return getOeuvreRequest(knownId, (res) => {
            console.log('Response recieved');
            const resultContent = JSON.parse(res.text);
            delete resultContent.src;
            expect(res, JSON.stringify(resultContent, function(k, v) {
                if (typeof k === 'src') {
                  return '...';
                } else if(v.length > 100) {
                    return '...'
                }
                return v;
              })).to.have.status(200)
        })
    })
    it('return http status 404 with unknown id', () => {
        return getOeuvreRequest(unknownId, (res) => {
            console.log('Response recieved');
            expect(res, res.text).to.have.status(404)
        })
    })
    it('return http status 400 with error with bad query param (not-string-id)', () => {
        return getOeuvreRequest(2, (res) => {
            console.log('Response recieved');
            expect(res, res.text).to.have.status(400)
            expect(res.text, res.text).to.contains('Wrong parameter id');
        })
    })
})