const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const expect = chai.expect;
const { ImageOeuvreFactory } = require('@legoya-website/oeuvre-test-datas');
const { authenticateAdmin, authenticateAnonymous } = require('../tools/requests');

chai.use(chaiHttp);

let token = '';

function addOeuvreRequest(body, tests, _token = token) {
    const path = `/api/v1/oeuvre`;
    console.log(`sending request to ${path} with token ${_token}`)
    return chai.request('127.0.0.1:7009')
        .post(path)
        .set('Authorization', `Bearer ${_token}`)
        .set('content-type', 'application/json')
        .send(body)
        .then((res) => {
            console.log(`recieve result for ${path}`)
            return tests(res)
        }, (err) => {
            console.log(`recieve error for ${path}`)
            assert(false, err.stack);
        });
}

describe('addOeuvre tests', function () {
    this.timeout(5000); 
    let knownId, unknownId;
    before(() => { 
        authenticateAnonymous().then((result) => { JSON.stringify(result.text) })
        return authenticateAdmin().then((result) => {
            token = JSON.parse(result.text).token;
        })
     })
    after(() => {  })

    it('return http status 201 with known id', () => {
        const factory = new ImageOeuvreFactory();
        const oeuvreList = factory.generateRandomOeuvreTransferModelList(factory.generateRandomUUIDList({ minOeuvres: 1, maxOeuvres: 1 }));
        return addOeuvreRequest(oeuvreList[0], (res) => {
            console.log('Response recieved');
            const resultContent = JSON.parse(res.text);
            delete resultContent.src;
            expect(res, JSON.stringify(resultContent, function(k, v) {
                if (typeof k === 'src') {
                  return '...';
                } else if(v.length > 100) {
                    return '...'
                }
                return v;
              })).to.have.status(201)
              expect(resultContent, res.text).to.have.property('id')
        })
    })
    it('return http status 400 with error with bad query param', () => {
        return addOeuvreRequest({ title: 'kukrapoc!' }, (res) => {
            console.log('Response recieved');
            expect(res, res.text).to.have.status(400)
            expect(res.text, res.text).to.contains('Missing required property');
        })
    })

    it('return http status 403 with anonymous token', () => {
        return authenticateAnonymous().then((result) => {
            const anonToken = JSON.parse(result.text).token;
            const factory = new ImageOeuvreFactory();
            const oeuvreList = factory.generateRandomOeuvreTransferModelList(factory.generateRandomUUIDList({ minOeuvres: 1, maxOeuvres: 1 }));
            return addOeuvreRequest(oeuvreList[0], (res) => {
                console.log('Response recieved');
                expect(res, res.text).to.have.status(403)
            }, anonToken)
        })
    })
})