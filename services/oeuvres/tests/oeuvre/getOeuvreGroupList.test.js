const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const expect = chai.expect;
const { ImageOeuvreFactory } = require('@legoya-website/oeuvre-test-datas');
const app = require('../../src/bin');
const { authenticateAdmin } = require('../tools/requests');

chai.use(chaiHttp);

function getOeuvreGroupListRequest(type, tests) {
    const path = `/api/v1/oeuvre/group/${type}`;
    console.log(`sending request to ${path}`)
    return chai.request('127.0.0.1:7009')
        .get(path)
        .set('Authorization', `Bearer ${token}`)
        .then((res) => {
            console.log(`recieve result for ${path}`)
            return tests(res)
        }, (err) => {
            console.log(`recieve error for ${path}`)
            assert(false, err.stack);
        });
}

let token = '';

describe('getOeuvreGroupList tests', function () {
    this.timeout(5000);
    before(() => {
        const factory = new ImageOeuvreFactory();
        const oeuvreList = factory.generateRandomOeuvreModelList(factory.generateRandomUUIDList({ minOeuvres: 100, maxOeuvres: 100 }));
        return Promise.all([
            app.modules.MongoRepositoryModule.models.Oeuvre.insertMany(oeuvreList),
            authenticateAdmin().then((result) => {
                token = JSON.parse(result.text).token;
            })
        ]);
    })
    after(() => {
        console.log('Deleting test datas')
        return app.modules.MongoRepositoryModule.models.Oeuvre.deleteMany();
    })
    it('return http status 200 with good query param (YEAR)', () => {
        return getOeuvreGroupListRequest('YEAR', (res) => {
            console.log('Response recieved');
            expect(res, res.text).to.have.status(200)
        })
    })
    it('return http status 200 with good query param (MONTH)', () => {
        return getOeuvreGroupListRequest('MONTH', (res) => {
            console.log('Response recieved');
            expect(res, res.text).to.have.status(200)
        })
    })
    /** Non implémenté à cet endroit car récupèrerait trop de données */
    it('return http status 503 with good query param (TAG)', () => {
        return getOeuvreGroupListRequest('TAG', (res) => {
            console.log('Response recieved');
            expect(res, res.text).to.have.status(503)
        })
    })
    it('return http status 400 with error with bad query param (KUKRAPOC)', () => {
        return getOeuvreGroupListRequest('KUKRAPOC', (res) => {
            console.log('Response recieved');
            expect(res, res.text).to.have.status(400)
            expect(res.text, res.text).to.contains('Wrong parameter group_type');
        })
    })
})