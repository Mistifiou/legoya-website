# Service Oeuvre

Service gérant les données relatives aux oeuvres.  

Temporairement, gère aussi l'authentification

## Architecture

L'idée globale tiens en deux points:
- Baser fortement l'application sur openapi
- Avoir un système me permettant de rajouter des modules, réutilisables

### Open API Based

Utilisation de oas-tools.

### Application modulaire

L'application charge un ensemble de modules communiquant entre eux (ou non).  
Un module a un cycle de vie, sinon c'est un outil simple.

## Modules

### Error handling

Gestion des erreurs globales. Eteind l'application en cas d'erreur non gérée.  

### Logger

Logger et LoggerFactory basé sur winston.  

### Mongo

Module de connection / chargement des modèles mongodb

### Oas

Module de gestion du http / express / oas

### Services

Charge la couche service.  
Plus globalement, charge de petits modules dans un namespace "service". 

