const OEUVRE_SCHEMA = function(Schema) {
    return {
        title: {
            type: String,
            required: true,
    
            uppercase: true,
            maxlength: 30
        },
        description: {
            type: String,
            required: true,
            maxlength: 500
        },
        created_at: {
            type: Date,
            required: true,
            default: Date.now
        },
        tags: {
            type: [String],
            required: true,
            index: true,
            maxlength: 30,
        },
        src: {
            type: Buffer,
            required: true
        },
        metadatas: {
            type: Schema.Types.Mixed,
            required: true
        }
    
    }
};

class OeuvreModelModule {

    constructor () {

    }

    load (mod) {
        return new Promise((resolve, reject) => {
            this.mongoose = mod.mongoose;
            const Schema = this.mongoose.Schema;
            this.schema = new Schema(OEUVRE_SCHEMA(Schema))
            this.model = this.mongoose.model('Oeuvre', this.schema);
            mod.models['Oeuvre'] = this.model;
            resolve();
        })
    }
}

module.exports = OeuvreModelModule;