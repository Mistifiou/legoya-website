const CONNEXION_SCHEMA = {
    _id: {
        type: String,
        required: true,
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now
    },
}

const ACCOUNT_SCHEMA = function(connexionSchema) {
    return {
        login: {
            type: String,
            required: true,
            unique: true,
            maxlength: 30
        },
        password: {
            type: String,
            required: true,
            minlength: 60,
            maxlength: 60
        },
        created_at: {
            type: Date,
            required: true,
            default: Date.now
        },
        role: {
            type: [String],
            required: true,
            enum: [
                'admin',
                'user'
            ]
        },
        connexions: {
            type: [connexionSchema],
            required: true,
        }
    }
};

class AccountModelModule {

    constructor () {

    }

    load (mod) {
        return new Promise((resolve, reject) => {
            this.mongoose = mod.mongoose;
            const Schema = this.mongoose.Schema;
            const connexionSchema = new Schema(CONNEXION_SCHEMA)
            this.schema = new Schema(ACCOUNT_SCHEMA(connexionSchema))
            this.model = this.mongoose.model('Account', this.schema);
            mod.models['Account'] = this.model;
            resolve();
        })
    }
}

module.exports = AccountModelModule;