
const EventEmitter = require('events');

class App extends EventEmitter {

    constructor () {
        super();
        this.modules = []
        this.on('error', (error) => {
            this._onFatalError(error)
        })
    }

    stop() {
        this.modules.OASTools.stop();
    }

    start () {
        if(!this.started) {
            this.started = true;
            this.ready = new Promise((resolve, reject) => {
                this.readyResolve = resolve;
            }).then(() => true);
            const modules = [
                './modules/logger.module',
                './modules/mongo-repository.module'
            ];
            if(process.env.NODE_ENV === 'prod') modules.push('./modules/global-error-handling.module')
            return this.loadModules(modules).then(() => {
                return this.loadModules([
                    './modules/services.module',
                    './modules/oas-tools.module'])
            }).then(() => {
                this.logger.info('----- All modules loaded');
                this.ready.then(() => this.ready = null)
                this.readyResolve();
            })
        } else {
            this.logger.warning('Application already loaded, not loading again')
            return Promise.resolve();
        }
    }

    loadModules(modules) {
        const promises = [];
        for (const modulePath of modules) {
            const mod = require(modulePath);
            if(this.logger) this.logger.info("-- Loading module " + mod.name +' from ' + modulePath)
            this.modules[mod.name] = new mod();
            promises.push(this.modules[mod.name].load(this));
        }
        return Promise.all(promises);
    }

    _onFatalError (err) {
        if(this.logger)  {
            this.logger.error('!!! An error occured in application context, restart needed !!!');
            this.logger.error(err.stack);
        } else {
            console.log('!!! An error occured in application context, restart needed !!!');
            console.log(err.message, err.stack);
        }
        if(process.env.NODE_ENV === 'prod') process.exit(1);
    }
}
const app = new App()

module.exports = app;