var jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const configuration = require('../tools/configuration').security

module.exports = class SecurityService {

    constructor() {

    }

    load(mod) {
        this.AccountModel = mod.getModel("Account");
    }

    authenticate(login, password) {
        if (login === 'anonymous') {
            try {
                const token = jwt.sign({
                    role: "anonymous"
                }, configuration.jwtKey, { expiresIn: 60 * 60 })

                return Promise.resolve(token)
            } catch (error) {
                return Promise.reject(error);
            }
        } else {
            return this.AccountModel
                .findOne({ login })
                .exec()
                .then((account) => {
                    if (!account) {
                        const err = new Error(`Account not found`)
                        err.code = 'NOT_FOUND'
                        throw err;
                    }
                    return bcrypt.compare(password, account.password).then((match) => {
                        if (!match) {
                            const err = new Error(`Account not found`)
                            err.code = 'NOT_FOUND'
                            throw err;
                        }
                        const token = jwt.sign({
                            accountId: account._id.toString,
                            role: account.role
                        }, configuration.jwtKey, { expiresIn: 60 * 60 })

                        return account.save().then(() => token)
                    })
                })
                .then((token) => {
                    return token
                })
        }
    }

    refreshToken(token) {
        return new Promise((resolve, reject) => {
            try {
                const bearerRegex = /^Bearer\s/;
                let newToken = token.replace(bearerRegex, '');
                newToken = jwt.decode(newToken, configuration.jwtKey, { json: true });
                delete newToken.exp;
                delete newToken.iat;
                newToken = jwt.sign(newToken, configuration.jwtKey, { expiresIn: 60 * 60 })
                return resolve(newToken);
            } catch (error) {
                reject(error);
            }
        })
    }

    verifyToken(token) {
        return new Promise((resolve, reject) => {
            const bearerRegex = /^Bearer\s/;
            if (token && bearerRegex.test(token)) {
                const newToken = token.replace(bearerRegex, '');
                jwt.verify(newToken, configuration.jwtKey,
                    {},
                    (error, decoded) => {
                        if (error) {
                            const err = new Error(`Token ${token} validation failed: ${error.message}`)
                            err.code = 'FORBIDDEN'
                            return reject(err);
                        }
                        return resolve(decoded);
                    }
                );
            } else {
                const err = new Error(`Token validation failed ${token}: bad token format`)
                err.code = 'FORBIDDEN'
                return reject(err);
            }
        })
    }
}