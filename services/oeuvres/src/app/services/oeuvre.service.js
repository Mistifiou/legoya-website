
const dayjs = require('dayjs');

module.exports = class OeuvreService {

    constructor() {

    }

    load(mod) {
        this.OeuvreModel = mod.getModel("Oeuvre");
    }

    getOeuvreGroupList (group) {
        /** Dans tout les cas on va vouloir récupérer les modèles */
        return this.OeuvreModel
            .find({}, [ "created_at", "tags"], {
                sort:{
                    date_added: -1 //Sort by Date Added DESC
                }
            })
            .select("created_at tags")
            .exec()
            .then((oeuvreList) => {      
                const functionKey = `_to${group}OeuvreGroupList`;
                if(this[functionKey]) {
                    return this[functionKey] (oeuvreList);
                } else {
                    const err = new Error(`Unknown group type ${group}`)
                    err.code = 'NOT_IMPLEMENTED'
                    throw err;
                }
        })
    }

    find (queryParameters) {
        const { title } = queryParameters
        return this.OeuvreModel.findOne({ title })
            .exec()
            .then((oeuvre) => {
                if(!oeuvre) {
                    const err = new Error(`Oeuvre not found with query parameters ${queryParameters}`)
                    err.code = 'NOT_FOUND'
                    throw err;
                }
                return oeuvre;
            })
    }

    getOeuvre (id) {
        return this.OeuvreModel.findOne({ _id: id })
            .exec()
            .then((oeuvre) => {
                if(!oeuvre) {
                    const err = new Error(`Oeuvre not found with id ${id}`)
                    err.code = 'NOT_FOUND'
                    throw err;
                }
                return oeuvre;
            })
    }

    createOeuvre (datas) {
        const oeuvre = new this.OeuvreModel(datas);
        return oeuvre.save().then(() => {
            return oeuvre;
        })
    }

    _toYEAROeuvreGroupList(oeuvreList) {
        const result = {};
        
        for(const oeuvre of oeuvreList) {
            const oeuvreYear = dayjs(oeuvre.created_at).format('YYYY');
            if(!result[oeuvreYear]) result[oeuvreYear] = this._createGroup(oeuvreYear);
            result[oeuvreYear].oeuvreList.push(oeuvre._id.toString());
        }

        return Object.keys(result)
            .sort((year1, year2) => parseInt(year1) < parseInt(year2) )
            .map((key) => result[key]);
    }
    
    _toMONTHOeuvreGroupList(oeuvreList) {
        const result = {};
        
        for(const oeuvre of oeuvreList) {
            const oeuvreYear = dayjs(oeuvre.created_at).format('YYYY-MM');
            if(!result[oeuvreYear]) result[oeuvreYear] = this._createGroup(oeuvreYear);
            result[oeuvreYear].oeuvreList.push(oeuvre._id.toString());
        }

        return Object.keys(result)
            .sort((year1, year2) => parseInt(year1) < parseInt(year2) )
            .map((key) => result[key]);
    }

    _createGroup(groupLabel) {
        return {
            groupLabel,
            oeuvreList: []
        }
    }
}