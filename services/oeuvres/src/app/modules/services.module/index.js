
const path = require('path');

class ServicesLoaderModule {

    constructor() {
        this.services = {};
    }

    load(app) {
        this.getModel = (name) => app.modules.MongoRepositoryModule.models[name]
        this.logger = app.Logger('Services loader module')
        return this.loadServices()
    }

    /** @see App */
    loadServices() {
        const promises = [];
        const modules = require(path.resolve('src/app/services'))
        for (const mod of modules) {
          this.logger.info("-- Loading service " + mod.name)
          this.services[mod.name] = new mod();
          promises.push(this.services[mod.name].load(this));
        }
        return Promise.all(promises);
    }
}

module.exports = ServicesLoaderModule;