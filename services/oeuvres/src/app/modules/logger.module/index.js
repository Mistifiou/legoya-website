const conf = require('../../tools/configuration').modules.logger;

class LoggerModule {

    constructor () {

    }

    load (app) {
        this.winston = require('winston');

        app.logger = this._getDefaultLogger();
        app.Logger = (name) => {
            return this._createLogger(name);
        }
    }

    _getDefaultLogger () {
        return this._createLogger('global logger');
    }

    _createLogger (name) {
        return this.winston.createLogger({
            defaultMeta: { name },
            level: conf.level,
            format: this.winston.format.combine(
                this.winston.format.timestamp(),
                this.winston.format.json(),
                this.winston.format.colorize(),
                this.winston.format(this._customLoggerFormat)(),
              ),
            transports: [
              new this.winston.transports.Console()
            ]
          });
    }

    _customLoggerFormat ({ level, name, message, timestamp }) {
        console.log(`${timestamp}[${level}][${name}] ${message} `)
        return `${timestamp}[${level}][${name}] ${message} `
    }
}

module.exports = LoggerModule;