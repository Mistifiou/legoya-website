
class GlobalErrorHandlingModule {

    constructor () {

    }

    load (app) {
        this.logger = app.Logger('global error handling module')

        this.logger.info('Listening on unhandled rejections');
        process.on('unhandledRejection', (reason, promise) => {
            this.logger.error('!!! Unhandled Rejection called !!!')
            app.emit('error', reason);
        });

        this.logger.info('Listening on uncaught exceptions');
        process.on('uncaughtException', (err) => {
            this.logger.error('!!! Uncaught Exception called !!!')
            app.emit('error', err);
        });
    }
}

module.exports = GlobalErrorHandlingModule;