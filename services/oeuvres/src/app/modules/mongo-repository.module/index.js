
const path = require('path');
const { Mongoose } = require('mongoose');
const configuration = require('../../tools/configuration').modules.mongoRepository

class MongoRepositoryModule {

    constructor() {
        this.mongoose = new Mongoose();
        this.models = {};
        this.modules = [];
    }

    load(app) {
        return new Promise((resolve, reject) => {
            if(app && app.Logger) {
                this.logger = app.Logger('mongo repository module');
                this.info = (...args) => this.logger.info(...args)
            } else {
                this.info = console.log
            }
            return this._connect()
                .then(() => {
                    return this.loadModules(path.resolve('src/app/models'))
                })
                .then(resolve)
        })
    }

    /** @see App */
    loadModules(path) {
        const promises = [];
        const modules = require(path)
        for (const mod of modules) {
            this.info("-- Loading model " + mod.name)
            this.modules[mod.name] = new mod();
            promises.push(this.modules[mod.name].load(this));
        }
        return Promise.all(promises);
    }
    _connect() {
        return new Promise((resolve, reject) => {
            if(process.env.NODE_ENV === "dev") {
                /* Connection en mémoire pour les dev et tests */
                this.info('Connecting to fake mongo server')
                const { MongoMemoryServer } = require('mongodb-memory-server');
                this.server = new MongoMemoryServer();
                this.server.getConnectionString().then((connString) => {
                    this.mongoose.connect(connString, { useNewUrlParser: true }, (err) => {
                    if(err) return reject(err)
                    resolve()
                });
                })
            } else {
                this.info('Connecting to mongo server')
                if(configuration.debug) {
                    this.info(`Connexion string: ${configuration.connectionString}`)
                }
                /* 'mongodb://username:password@host:port/database?options...' */
                this.mongoose.connect(configuration.connectionString, { useNewUrlParser: true }, (err) => {
                    if(err) return reject(err)
                    resolve()
                });
            }
        })
    }
}

module.exports = MongoRepositoryModule;