

const fs = require('fs');
const path = require('path');
const jsyaml = require('yamljs');
const oasTools = require('oas-tools');
const express = require('express');
const oasDoc = jsyaml.load(path.resolve('api/openapi.yaml'));
const bodyParser = require('body-parser')
const { verifyToken } = require(path.resolve('src/app/controllers/securityController'))
const config = require('../../tools/configuration').modules.oasTools
const cors = require('cors')

class OASTools {

  constructor() {
    this.controllers = [];
  }

  load(app) {
    return new Promise((resolve, reject) => {
      this.logger = app.Logger('express open api spec tools module');
      this.logger.warning = this.logger.warn;
      this.expressApp = express();
      oasDoc.servers[0].url = oasDoc.servers[0].url.replace('$BASE_URL', config.baseUrl)
      const options = {
        controllers: path.resolve('src/app/controllers'),
        checkControllers: true,
        loglevel: 'debug',
        customLogger: this.logger,
        strict: true,
        router: true,
        validator: true,
        docs: {
          apiDocs: '/api-docs',
          apiDocsPrefix: '',
          swaggerUi: '/docs',
          swaggerUiPrefix: ''
        },
        ignoreUnknownFormats: false,
        /** Securité @see https://www.npmjs.com/package/accesscontrol */
        oasSecurity: true,
        oasAuth: true,

        securityFile: {
          Bearer: (req, secDef, token, next) => verifyToken(req, req.res, next)
        },
        grantsFile: {
          Bearer: {
            anonymous: {
              "/oeuvre/group": {
                "read:any": ["*"]
              },
              "/oeuvre": {
                "read:any": ["*"]
              },
              "/token": {
                "update:any": ["*"]
              }
            },
            user: {
              "$extend": ["anonymous"],
            },
            admin: {
              "$extend": ["user"],
              "/oeuvre": {
                "create:any": ['*']
              }
            }
          }
        },
      };
      this.expressApp.use(bodyParser.json({ limit: '50mb', extended: true }))
      /** @see https://www.npmjs.com/package/cors#configuring-cors-w-dynamic-origin */
      const whitelist = [
        'https://legoya.fr', 
        'https://www.legoya.fr', 
        /** @todo: remonter en config la whitelist */
        'https://legoya-prd-client.sys.legoya.fr', 
        'https://legoya-prd-service-oeuvre.sys.legoya.fr',
        config.baseUrl
      ]
      const corsOptions = {
        origin: (origin, callback) => {
          if (whitelist.indexOf(origin) !== -1 || !origin) {
            callback(null, true)
          } else {
            callback(new Error('Not allowed by CORS'))
          }
        }
      }
      this.expressApp.use(cors(corsOptions))
      oasTools.configure(options);
      oasTools.initialize(oasDoc, this.expressApp, () => {
        this.server = this.expressApp.listen(config.port, () => {
          this.logger.info(`App up and running! listening localy on ${config.port}. Available at ${oasDoc.servers[0].url}`);
          this.logger.info(`Swagger UI available at ${config.baseUrl}/docs`)
          resolve();
        });
      });
    })
  }

  stop() {
    this.server.close()
  }
}

module.exports = OASTools;