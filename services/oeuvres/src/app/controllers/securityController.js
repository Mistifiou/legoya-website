
const app = require('../index');
const { STATUS } = require('./constants')

const SecurityService = app.modules.ServicesLoaderModule.services.SecurityService;

class SecurityController {
        
    static authenticate (req, res, next) {
        try {
            return SecurityService.authenticate(req.body.login, req.body.password).then((token) => {
                res.status(201).send({ token });
            }).catch((err) => {
                if(err.code) {
                    res.status(STATUS[err.code]).json([{ message: err.message, error: { code: err.code } }, err.stack])
                } else {
                    res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
                }
            })
        } catch (err) {
            res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
        }
    }
    
    static refreshToken (req, res, next) {
        try {
            return SecurityService.refreshToken(req.header('Authorization')).then((token) => {
                res.status(200).send({ token });
            }).catch((err) => {
                if(err.code) {
                    res.status(STATUS[err.code]).json([{ message: err.message, error: { code: err.code } }, err.stack])
                } else {
                    res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
                }
            })
        } catch (err) {
            res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
        }
    }
    
    static verifyToken(req, res, next) {
        try {
            return SecurityService.verifyToken(req.header('Authorization')).then((token) => {
                return next();
            }).catch((err) => {
                if(err.code) {
                    res.status(STATUS[err.code]).json([{ message: err.message, error: { code: err.code } }, err.stack])
                } else {
                    res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
                }
            })
        } catch (err) {
            res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
        }
    }
}

module.exports = SecurityController;