
module.exports = {
    STATUS: {
        NOT_IMPLEMENTED: 503,
        NOT_FOUND: 404,
        FORBIDDEN:401
    }
}