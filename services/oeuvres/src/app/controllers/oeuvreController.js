
const app = require('../index');
const dayjs = require('dayjs');
const btoa = require('btoa');
const { STATUS } = require('./constants')

const OeuvreService = app.modules.ServicesLoaderModule.services.OeuvreService;


class OeuvreController {

    static find(req, res, next) {
        try {
            console.log(req.query)
            return OeuvreService.find(req.query).then((oeuvre) => {
                res.status(200).send({
                    id: oeuvre._id.toString(),
                    title: oeuvre.title,
                    description: oeuvre.description,
                    tags: oeuvre.tags,
                    created_at: dayjs(oeuvre.created_at).format('YYYY-MM-DD'),
                    src: btoa(oeuvre.src),
                    metadatas: oeuvre.metadatas || {}
                });
            }).catch((err) => {
                if(err.code) {
                    res.status(STATUS[err.code]).json([{ message: err.message, error: { code: err.code } }, err.stack])
                } else {
                    res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
                }
            })
        } catch (err) {
            res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
        }
    }
        
    static getOeuvre(req, res, next) {
        try {
            return OeuvreService.getOeuvre(req.params.id).then((oeuvre) => {
                res.status(200).send({
                    id: oeuvre._id.toString(),
                    title: oeuvre.title,
                    description: oeuvre.description,
                    tags: oeuvre.tags,
                    created_at: dayjs(oeuvre.created_at).format('YYYY-MM-DD'),
                    src: btoa(oeuvre.src),
                    metadatas: oeuvre.metadatas || {}
                });
            }).catch((err) => {
                if(err.code) {
                    res.status(STATUS[err.code]).json([{ message: err.message, error: { code: err.code } }, err.stack])
                } else {
                    res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
                }
            })
        } catch (err) {
            res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
        }
    }
    
    static getOeuvreGroupList(req, res, next) {
        try {
            return OeuvreService.getOeuvreGroupList(req.params.group_type).then((oeuvreList) => {
                res.status(200).send(oeuvreList);
            }).catch((err) => {
                if(err.code) {
                    res.status(STATUS[err.code]).json([{ message: err.message, error: { code: err.code } }, err.stack])
                } else {
                    res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
                }
            })
        } catch (err) {
            res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
        }
    }
    
    static addOeuvre(req, res, next) {
        try {
            req.body.src = Buffer.from(req.body.src, 'base64');
            return OeuvreService.createOeuvre(req.body).then((oeuvre) => {
                res.status(201).send({id: oeuvre.id});
            }).catch((err) => {
                if(err.code) {
                    res.status(STATUS[err.code]).json([{ message: err.message, error: { code: err.code } }, err.stack])
                } else {
                    res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
                }
            })
        } catch (err) {
            res.status(500).json([{ message: err.message, error: { code: 'SERVER_ERROR' } }, err.stack])
        }
    }
}

module.exports = OeuvreController;