let configuration = {
    dev: {
        modules: {
            logger: {
                level: 'debug'
            },
            mongoRepository: {
                connectionString: 'mongodb://oeuvreService:root@mongo/legoya-website'
            },
            oasTools: {
                port: 7009,
                baseUrl: 'http://127.0.0.1:7009'
            }
        },
        security: {
            jwtKey: 'NOT_SO_HARD'
        }
    },
    int: {
        modules: {
            mongoRepository: {
                connexionString: 'unknown'
            },
            logger: {
                level: 'debug'
            },
            oasTools: {
                port: 7009,
                baseUrl: 'http://127.0.0.1:7009'
            }
        },
        security: {
            jwtKey: 'jwtKey'
        }
    },
    prod: {
        modules: {
            mongoRepository: {
                connexionString: 'unknown'
            },
            oasTools: {
                baseUrl: 'https://legoya-prd-service-oeuvre.sys.legoya.fr',
                port: 7009,
            },
            logger: {
                level: 'info'
            },
        },
        security: {
            jwtKey: 'ERR'
        }
    }
}

console.log(`Loading configuration from ${process.env.NODE_ENV}`);
configuration = configuration[process.env.NODE_ENV];

if(process.env.DB_CONNECTION_STRING) configuration.modules.mongoRepository.connectionString = process.env.DB_CONNECTION_STRING;
if(process.env.PORT) configuration.modules.oasTools.port = process.env.PORT;
if(process.env.JWT_KEY) configuration.security.jwtKey = process.env.JWT_KEY;
if(process.env.BASE_URL) configuration.modules.oasTools.baseUrl = process.env.BASE_URL;
if(process.env.LOG_LEVEL) configuration.modules.logger.level = process.env.LOG_LEVEL;

module.exports = configuration;