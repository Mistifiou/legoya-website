###########################################
## DOCKERFILE CLIENT ANGULAR
###########################################
## ENV PROD
###########################################

FROM node:12.2.0

ENV NODE_ENV="dev"
ENV CLI_PATH="clients/angular-cli"

# C'est moche de tout copier à l'arrache si l'étape de build dans dans un dockerfile
# A améliorer
COPY . .

RUN echo "unsafe-perm=true" >> ~/.npmrc
RUN npm run prepare:ci
RUN lerna bootstrap --no-ci --force-local

WORKDIR $CLI_PATH

RUN npm i -g angular-http-server
RUN ng build -c dev
CMD angular-http-server --path dist/angular-cli

EXPOSE 8080