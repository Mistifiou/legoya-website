###########################################
## DOCKERFILE SERVICE OEUVRE
###########################################
## ENV *
###########################################

FROM node:12.2.0

ENV SERV_PATH "services/oeuvres"
ENV APP_ENV="dev"
ENV PORT="7009"

# C'est moche de tout copier à l'arrache si l'étape de build dans dans un dockerfile
# A améliorer
COPY . .

RUN echo "unsafe-perm=true" >> ~/.npmrc
RUN npm run prepare:ci
RUN lerna bootstrap --no-ci --force-local

WORKDIR $SERV_PATH

ENV NODE_ENV=$APP_ENV

CMD npm run serve

EXPOSE 7009