import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { CommonViewsModule } from 'src/views/common/common.module';
import { HttpService } from 'src/services/http/http.service';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        CommonViewsModule,
        HttpClientModule
      ],
      declarations: [
        AppComponent,
      ],
      providers: [
        HttpService,
        {provide: 'IConnectionService', useClass: environment.IConnectionServiceType},
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Legoya's artbook'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Legoya\'s artbook');
  });
});
