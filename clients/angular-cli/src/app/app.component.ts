import { Component, OnInit } from '@angular/core';
import { ConnectionService } from 'src/services/connexion/connection.service';
import { AppStoreService } from 'src/services/app/store/app-store.service';
import { IConnection } from 'src/services/connexion/connection-service-interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Legoya\'s artbook';
  connection: IConnection;

  get loaded() {
    return this.connection && this.connection.active;
  }

  constructor(private store: AppStoreService) {

  }

  ngOnInit() {
    this.store.connection.subscribe((nextVal) => {
      this.connection = nextVal;
    });

  }
}
