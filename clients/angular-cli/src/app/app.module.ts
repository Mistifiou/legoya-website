import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonViewsModule } from 'src/views/common/common.module';
import { ConnectionService } from 'src/services/connexion/connection.service';
import { AppStoreService } from 'src/services/app/store/app-store.service';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MarkdownModule } from 'ngx-markdown';
import { Location } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonViewsModule,
    HttpClientModule,
    MarkdownModule.forRoot()
  ],
  providers: [
    {provide: 'IConnectionService', useClass: environment.IConnectionServiceType},
    AppStoreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
