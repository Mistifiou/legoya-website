import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OeuvreVignetteComponent } from './oeuvre-vignette/oeuvre-vignette.component';
import { OeuvreGroupeComponent } from './oeuvre-groupe/oeuvre-groupe.component';
import { CommonViewsModule } from '../common/common.module';
import { MarkdownModule } from 'ngx-markdown';



@NgModule({
  declarations: [OeuvreVignetteComponent, OeuvreGroupeComponent],
  exports: [OeuvreVignetteComponent, OeuvreGroupeComponent],
  imports: [
    CommonModule,
    CommonViewsModule,
    MarkdownModule.forChild()
  ]
})
export class OeuvresViewsModule { }
