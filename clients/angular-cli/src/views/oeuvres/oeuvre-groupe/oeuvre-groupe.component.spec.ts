import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OeuvreGroupeComponent } from './oeuvre-groupe.component';
import { OeuvreVignetteComponent } from '../oeuvre-vignette/oeuvre-vignette.component';
import { CommonViewsModule } from 'src/views/common/common.module';

describe('OeuvreGroupeComponent', () => {
  let component: OeuvreGroupeComponent;
  let fixture: ComponentFixture<OeuvreGroupeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonViewsModule
      ],
      declarations: [ OeuvreGroupeComponent, OeuvreVignetteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OeuvreGroupeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
