import { Component, OnInit, Input } from '@angular/core';
import { IOeuvreGroupeViewModel } from '@legoya-website/oeuvre-vm';

/**
 * @todo: Brancher la date en haut
 * @todo: Gérer le actif / inactif: Trouver un meilleur enchainement à l'ouverture d'un detail
 */
@Component({
  selector: 'app-oeuvre-groupe',
  templateUrl: './oeuvre-groupe.component.html',
  styleUrls: ['./oeuvre-groupe.component.scss']
})
export class OeuvreGroupeComponent implements OnInit {

  @Input() oeuvreGroupe: IOeuvreGroupeViewModel;

  constructor() {
  }

  ngOnInit() {
  }
}
