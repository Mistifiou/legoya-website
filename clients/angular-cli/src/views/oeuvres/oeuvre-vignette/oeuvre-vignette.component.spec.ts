import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OeuvreVignetteComponent } from './oeuvre-vignette.component';

describe('OeuvreVignetteComponent', () => {
  let component: OeuvreVignetteComponent;
  let fixture: ComponentFixture<OeuvreVignetteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OeuvreVignetteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OeuvreVignetteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
