import { Component, OnInit, Input, HostBinding, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { IOeuvreViewModel } from '@legoya-website/oeuvre-vm';

/**
 * @todo: Terminer le layout de la partie "détails" et intégrer vue plein écran
 * @todo: Gérer particularité format portrait / paysage
 * @todo:
 */
@Component({
  selector: 'app-oeuvre-vignette',
  templateUrl: './oeuvre-vignette.component.html',
  styleUrls: ['./oeuvre-vignette.component.scss']
})
export class OeuvreVignetteComponent implements OnInit {

  @Input() oeuvre: IOeuvreViewModel;
  @HostBinding('class.active') active = false;
  @HostBinding('class.zoom-in') zoomIn = false;

  @Input('zoom-in') set zoom(zoom: boolean) {
    this.zoomIn = zoom;
  }

  @Output() dismiss: EventEmitter<void> = new EventEmitter();
  @ViewChild('vignette', {static: false}) vignette: ElementRef;

  constructor() { }

  get title() {
    if (this.oeuvre) {
      return this.oeuvre.title.replace(/[ ]/g, '-');
    }
    return '';
  }

  ngOnInit() {
  }

  onclick(/* vignette: HTMLElement */) {
    if (!this.zoomIn && !this.active) {
      this.active = true;
      this.onZoomIn();
    } else if (this.zoomIn) {
      this.onZoomOut();
      setTimeout(() => {
        const scroller = this.vignette.nativeElement.parentElement.parentElement ?
          this.vignette.nativeElement.parentElement.parentElement.parentElement :
          this.vignette.nativeElement.parentElement;
        scroller.scrollTo(0, scroller.scrollTop + this.vignette.nativeElement.getBoundingClientRect().top - 60);
      }, 0);
    } else if (this.active) {
      this.active = false;
    }
  }

  onZoomIn() {
    this.zoomIn = true;
    this.active = false;
  }

  onZoomOut() {
    this.zoomIn = false;
    this.active = true;
    this.dismiss.emit();
  }

  onZoom() {
    this.zoomIn = !this.zoomIn;
    this.active = false;
  }
}
