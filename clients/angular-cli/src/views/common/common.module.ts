import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { LayerViewComponent } from './layer-view/layer-view.component';



@NgModule({
  declarations: [HeaderComponent, LayerViewComponent],
  imports: [
    CommonModule
  ],
  exports: [HeaderComponent, LayerViewComponent]
})
export class CommonViewsModule { }
