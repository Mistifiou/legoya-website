import { TestBed } from '@angular/core/testing';

import { OeuvreStoreService } from './oeuvre-store.service';
import { environment } from 'src/environments/environment';

describe('OeuvreStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: 'IOeuvreClient', useClass: environment.IOeuvreClientType}
    ],
  }));

  it('should be created', () => {
    const service: OeuvreStoreService = TestBed.get(OeuvreStoreService);
    expect(service).toBeTruthy();
  });
});
