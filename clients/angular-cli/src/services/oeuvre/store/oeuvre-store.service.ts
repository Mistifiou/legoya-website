import { Injectable, Inject, EventEmitter } from '@angular/core';
import { IOeuvreGroupeViewModel, IOeuvreViewModel } from '@legoya-website/oeuvre-vm';

import { IOeuvreClient, OEUVRE_GROUP_TYPE } from '../client/oeuvre-client-interface';
import { BehaviorSubject } from 'rxjs';
import { IOeuvreGroupeTransferModel } from '@legoya-website/oeuvre-tm';

interface IOeuvreStoreServiceState {
  groups: Map<OEUVRE_GROUP_TYPE, BehaviorSubject<Array<IOeuvreGroupeViewModel>>>;
  oeuvres: Map<string, IOeuvreViewModel>;
  last_updates: {
    groups: Map<OEUVRE_GROUP_TYPE, number>
  };
}

const GROUP_RELOAD_TIMEOUT = 5000; // ms

/**
 * @todo: relier aux modèles de l'application
 * @todo: relier au back office quand il sera monté
 */
@Injectable({
  providedIn: 'root'
})
export class OeuvreStoreService {

  public allLoaded: EventEmitter<any> = new EventEmitter();

  constructor(@Inject('IOeuvreClient') public clientService: IOeuvreClient) {
    this.state.groups.set(OEUVRE_GROUP_TYPE.BY_YEAR, new BehaviorSubject<Array<IOeuvreGroupeViewModel>>([]));
  }

  private state: IOeuvreStoreServiceState = {
    groups: new Map<OEUVRE_GROUP_TYPE, BehaviorSubject<Array<IOeuvreGroupeViewModel>>>(),
    oeuvres: new Map<string, IOeuvreViewModel>(),
    last_updates: {
      groups: new Map<OEUVRE_GROUP_TYPE, number>()
    }
  };

  public groups(type: OEUVRE_GROUP_TYPE): BehaviorSubject<Array<IOeuvreGroupeViewModel>> {
    this._tryUpdateGroup(type);
    return this.state.groups.get(type);
  }

  public oeuvreByTitle(title: string): Promise<IOeuvreViewModel> {
    return this.clientService.getOeuvreBy({ title })
      .then((oeuvre) => {
        return {
          created_at: oeuvre.created_at,
          description: oeuvre.description,
          metadatas: oeuvre.metadatas,
          src: oeuvre.src,
          tags: oeuvre.tags,
          title: oeuvre.title
        };
      });
  }



  private _tryUpdateGroup(type: OEUVRE_GROUP_TYPE) {
    if (
      !this.state.last_updates.groups[type] ||
      this.state.last_updates.groups[type] + GROUP_RELOAD_TIMEOUT > Date.now()) {

      this.clientService.findGroups({ grouped_by: type }).then((result: Array<IOeuvreGroupeTransferModel>) => {
        this.state.last_updates.groups.set(type, Date.now());
        this._mapGroupList(type, result);
      }).catch((err) => {
        console.error(err);
      });
    }
  }

  private _mapGroupList(type: OEUVRE_GROUP_TYPE, groupList: Array<IOeuvreGroupeTransferModel>) {

    const mappedList: Array<IOeuvreGroupeViewModel> = [];
    const allLoad = [];
    for (const group of groupList) {
      const groupVM: IOeuvreGroupeViewModel = {
        groupLabel: group.groupLabel,
        oeuvreList: []
      };
      mappedList.push(groupVM); 
      for (const oeuvreId of group.oeuvreList) {
        allLoad.push(this.clientService.getOeuvre(oeuvreId).then((oeuvre) => {
          groupVM.oeuvreList.push({
            created_at: oeuvre.created_at,
            description: oeuvre.description,
            metadatas: oeuvre.metadatas,
            src: oeuvre.src,
            tags: oeuvre.tags,
            title: oeuvre.title
          });
          this.state.groups.get(type).next(mappedList);
        }).catch((err) => {
          console.log(err);
        }));
      }
    }
    Promise.all(allLoad).then(() => this.allLoaded.emit())
  }
}
