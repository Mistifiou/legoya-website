import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OeuvreStoreService } from './store/oeuvre-store.service';
import { environment } from 'src/environments/environment';



@NgModule({
  declarations: [  ],
  providers: [
    OeuvreStoreService,
    {provide: 'IOeuvreClient', useClass: environment.IOeuvreClientType}
  ],
  imports: [
    CommonModule
  ]
})
export class OeuvreServicesModule { }
