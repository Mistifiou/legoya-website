import { TestBed } from '@angular/core/testing';

import { OeuvresFakeHttpClientService } from './oeuvres-fake-http-client.service';

describe('OeuvresFakeHttpClientService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OeuvresFakeHttpClientService = TestBed.get(OeuvresFakeHttpClientService);
    expect(service).toBeTruthy();
  });
});
