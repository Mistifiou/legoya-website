import { TestBed } from '@angular/core/testing';

import { OeuvresHttpClientService } from './oeuvres-http-client.service';
import { HttpService } from 'src/services/http/http.service';
import { HttpClientModule } from '@angular/common/http';

describe('OeuvresHttpClientService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ],
    providers: [
      HttpService
    ]
  }));

  it('should be created', () => {
    const service: OeuvresHttpClientService = TestBed.get(OeuvresHttpClientService);
    expect(service).toBeTruthy();
  });
});
