import { IOeuvreGroupeTransferModel, IOeuvreTransferModel } from '@legoya-website/oeuvre-tm';
import { UUID } from '@legoya-website/types';

export enum OEUVRE_GROUP_TYPE {
    BY_MONTH= 'MONTH',
    BY_YEAR= 'YEAR',
    BY_TAG= 'TAG'
}

export interface IgetOeuvreGroupeListOptions {
    grouped_by: OEUVRE_GROUP_TYPE;
}

export interface IOeuvreClient {

    getOeuvreBy(byOpts): Promise<IOeuvreTransferModel>;
    findGroups(opts: IgetOeuvreGroupeListOptions): Promise<Array<IOeuvreGroupeTransferModel>>;
    getOeuvre(id: UUID): Promise<IOeuvreTransferModel>;
}
