import { Injectable } from '@angular/core';
import { IOeuvreClient, IgetOeuvreGroupeListOptions } from './oeuvre-client-interface';
import { IOeuvreTransferModel, IOeuvreGroupeTransferModel } from '@legoya-website/oeuvre-tm';
import { HttpService } from 'src/services/http/http.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OeuvresHttpClientService implements IOeuvreClient {

  constructor(private httpService: HttpService) { }

  getOeuvreBy(opts): Promise<IOeuvreTransferModel> {
    return this.httpService
      .get<IOeuvreTransferModel>(`${environment.oeuvreServiceBaseUrl}/oeuvre?${this._toUrlParams(opts)}`);
  }

  findGroups(opts: IgetOeuvreGroupeListOptions): Promise<IOeuvreGroupeTransferModel[]> {
    return this.httpService
      .get<IOeuvreGroupeTransferModel[]>(`${environment.oeuvreServiceBaseUrl}/oeuvre/group/${opts.grouped_by}`)
      .then((res: IOeuvreGroupeTransferModel[]) => {
        return res;
      });
  }
  getOeuvre(id: string): Promise<IOeuvreTransferModel> {
    return this.httpService
      .get<IOeuvreTransferModel>(`${environment.oeuvreServiceBaseUrl}/oeuvre/${id}`)
      .then((res: IOeuvreTransferModel) => {
        return res;
      });
  }

  _toUrlParams(params: any) {
    return Object.keys(params).map((key) => {
      return `${key}=${params[key]}`;
    }).join('&');
  }
}
