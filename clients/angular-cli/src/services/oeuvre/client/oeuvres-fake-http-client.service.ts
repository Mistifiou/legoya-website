import { Injectable } from '@angular/core';
import { IOeuvreClient, IgetOeuvreGroupeListOptions } from './oeuvre-client-interface';
import { IOeuvreGroupeTransferModel, IOeuvreTransferModel } from '@legoya-website/oeuvre-tm';
import { ImageOeuvreFactory } from '@legoya-website/oeuvre-test-datas';

@Injectable({
  providedIn: 'root'
})
export class OeuvresFakeHttpClientService implements IOeuvreClient {
  constructor() {
  }

  getOeuvreBy(byOpts): Promise<IOeuvreTransferModel> {
    return new Promise((resolve, reject) => {
      try {
        const factory = new ImageOeuvreFactory();
        const result = factory.generateRandomOeuvreList(['random XP']);
        resolve(result[0]);
      } catch (error) {
        reject(error);
      }
    });
  }

  findGroups(opts: IgetOeuvreGroupeListOptions): Promise<IOeuvreGroupeTransferModel[]> {
    return new Promise((resolve, reject) => {
      try {
        const factory = new ImageOeuvreFactory();
        const result = factory.generateGroupViewDataList({ groupCount: 3, perGroup: { minOeuvres: 12 , maxOeuvres: 12 }});
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  getOeuvre(id: string): Promise<IOeuvreTransferModel> {
    return new Promise((resolve, reject) => {
      try {
        const factory = new ImageOeuvreFactory();
        const result = factory.generateRandomOeuvreList([id]);
        resolve(result[0]);
      } catch (error) {
        reject(error);
      }
    });
  }
}
