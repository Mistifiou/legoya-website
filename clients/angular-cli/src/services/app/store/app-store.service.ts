import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IConnection, IConnectionService } from 'src/services/connexion/connection-service-interface';

interface IAppState {
  connection: BehaviorSubject<IConnection>;
}

@Injectable({
  providedIn: 'root'
})
export class AppStoreService {

  private state: IAppState;

  constructor(@Inject('IConnectionService') public connectionService: IConnectionService) {
    this.state = {
      connection: new BehaviorSubject<IConnection>({ active: false })
    };
  }

  get connection() {
    if (!this.state.connection.getValue().active) { this._connectAnonymous(); }
    return this.state.connection;
  }

  private _connectAnonymous() {
    this.connectionService.connectAnonymous().then((connection: IConnection) => {
      this.state.connection.next(connection);
    });
  }
}
