import { TestBed } from '@angular/core/testing';

import { AppStoreService } from './app-store.service';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';

describe('AppStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ],
    providers: [
      {provide: 'IConnectionService', useClass: environment.IConnectionServiceType},
    ]
  }));

  it('should be created', () => {
    const service: AppStoreService = TestBed.get(AppStoreService);
    expect(service).toBeTruthy();
  });
});
