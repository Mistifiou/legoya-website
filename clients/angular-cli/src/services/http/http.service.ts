import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';

interface IHTTPOpts {
  headers: HttpHeaders;
}

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  httpOpts: IHTTPOpts = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      Authorization: ''
    })
  };

  constructor(private http: HttpClient) { }

  setAuthToken(token: string) {
    this.httpOpts.headers = this.httpOpts.headers.set('Authorization', `Bearer ${token}`);
  }

  get<T>(url: string): Promise<T> {
    return this.http
      .get<T>(url, this.httpOpts)
      .toPromise();
  }

  post<T>(url: string, body: any): Promise<T> {
    return this.http
      .post<T>(url, body, this.httpOpts)
      .toPromise()
      .then((result: any) => {
        return result as T;
      });
  }

}
