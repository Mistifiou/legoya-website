import { TestBed } from '@angular/core/testing';

import { ConnectionService } from './connection.service';
import { HttpService } from '../http/http.service';
import { HttpClientModule } from '@angular/common/http';

describe('ConnectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ],
    providers: [
      HttpService
    ]
  }));

  it('should be created', () => {
    const service: ConnectionService = TestBed.get(ConnectionService);
    expect(service).toBeTruthy();
  });
});
