import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { environment } from '../../environments/environment';
import * as jwt_decode from 'jwt-decode';
import { IConnectionService, IConnection, IAuthenticateResponse } from './connection-service-interface';


@Injectable({
  providedIn: 'root'
})
export class ConnectionService implements IConnectionService {

  constructor(private httpService: HttpService) { }

  connectAnonymous(): Promise<IConnection> {
    return this.httpService
      .post<IAuthenticateResponse>(`${environment.oeuvreServiceBaseUrl}/authenticate`, {
        login: 'anonymous',
        password: 'anonymous'
      })
      .then((res: IAuthenticateResponse) => {
        this.httpService.setAuthToken(res.token);
        const decrypted = jwt_decode(res.token);
        console.log(decrypted);
        return {
          active: true,
          token: res.token
        };
      });
  }
}
