import { IOeuvreGroupeTransferModel, IOeuvreTransferModel } from '@legoya-website/oeuvre-tm';
import { UUID } from '@legoya-website/types';

export interface IConnection {
    active: boolean;
    token?: string;
    datas?: any;
  }

export interface IAuthenticateResponse {
    token: string;
  }

export interface IConnectionService {

    connectAnonymous(): Promise<IConnection>;
}
