import { Injectable } from '@angular/core';
import { IConnectionService, IConnection } from './connection-service-interface';




@Injectable({
  providedIn: 'root'
})
export class FakeConnectionService implements IConnectionService {

  constructor() { }

  connectAnonymous(): Promise<IConnection> {
    return Promise.resolve({
      active: true,
      token: 'fake'
    });
  }
}
