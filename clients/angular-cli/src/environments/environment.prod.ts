import { OeuvresHttpClientService } from 'src/services/oeuvre/client/oeuvres-http-client.service';
import { ConnectionService } from 'src/services/connexion/connection.service';

export const environment = {
  production: true,
  IOeuvreClientType: OeuvresHttpClientService,
  IConnectionServiceType: ConnectionService,
  // @ts-ignore
  oeuvreServiceBaseUrl: 'https://legoya-prd-service-oeuvre.sys.legoya.fr/api/v1',
};
