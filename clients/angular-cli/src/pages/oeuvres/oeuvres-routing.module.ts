import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultationOeuvresComponent } from 'src/pages/oeuvres/consultation-oeuvres/consultation-oeuvres.component';


const routes: Routes = [
  { path: '', component: ConsultationOeuvresComponent },
  { path: ':id', component: ConsultationOeuvresComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OeuvresRoutingModule { }
