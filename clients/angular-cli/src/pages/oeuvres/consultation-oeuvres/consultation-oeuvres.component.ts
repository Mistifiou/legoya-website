import { Component, OnInit } from '@angular/core';
import { IOeuvreGroupeViewModel, IOeuvreViewModel } from '@legoya-website/oeuvre-vm';
import { OeuvreStoreService } from 'src/services/oeuvre/store/oeuvre-store.service';
import { OEUVRE_GROUP_TYPE } from 'src/services/oeuvre/client/oeuvre-client-interface';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-consultation-oeuvres',
  templateUrl: './consultation-oeuvres.component.html',
  styleUrls: ['./consultation-oeuvres.component.scss']
})
export class ConsultationOeuvresComponent implements OnInit {

  imageGroupedList: Array<IOeuvreGroupeViewModel>;
  currentOeuvre: IOeuvreViewModel;
  loadedOnce: boolean;

  constructor(public oeuvreStore: OeuvreStoreService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.oeuvreStore.allLoaded.subscribe(() => this.loadScript())
    this.route.fragment.subscribe(oeuvreTitre => {
      if (oeuvreTitre) {
        /** format de l'anchor: mon-titre-pour-url */
        oeuvreTitre = oeuvreTitre.replace('-', ' ');

        /** On peu avoir un identifiant prioritaire à afficher en full-page */
        this.oeuvreStore.oeuvreByTitle(oeuvreTitre).then((oeuvre) => {
          this.currentOeuvre = oeuvre;
        });
      }
      this.oeuvreStore.groups(OEUVRE_GROUP_TYPE.BY_YEAR).asObservable().subscribe((datas) => {
        this.imageGroupedList = datas
          .sort((a, b) => {
            if (a.groupLabel < b.groupLabel) {
              return 1;
            } else if (a.groupLabel > b.groupLabel) {
              return -1;
            }
            return 0;
          }).map((group) => {
            group.oeuvreList = group.oeuvreList.sort((a, b) => {
              if (a.created_at < b.created_at) {
                return 1;
              } else if (a.created_at > b.created_at) {
                return -1;
              }
              return 0;
            });
            return group;
          });
      });
    });
  }

  public loadScript() {
      const body = document.body;
      const script = document.createElement('script');
      script.innerHTML = '';
      script.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v4.0';
      script.async = true;
      script.defer = true;
      script.crossOrigin = 'anonymous';
      body.appendChild(script);
  }
}
