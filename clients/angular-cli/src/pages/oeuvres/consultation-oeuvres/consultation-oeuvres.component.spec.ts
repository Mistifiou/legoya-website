import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultationOeuvresComponent } from './consultation-oeuvres.component';
import { OeuvresViewsModule } from 'src/views/oeuvres/oeuvres-views.module';
import { OeuvreServicesModule } from 'src/services/oeuvre/oeuvre-services.module';
import { RouterModule } from '@angular/router';

describe('ConsultationOeuvresComponent', () => {
  let component: ConsultationOeuvresComponent;
  let fixture: ComponentFixture<ConsultationOeuvresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        OeuvresViewsModule,
        OeuvreServicesModule,
        RouterModule.forRoot([]),
      ],
      declarations: [ ConsultationOeuvresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultationOeuvresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
