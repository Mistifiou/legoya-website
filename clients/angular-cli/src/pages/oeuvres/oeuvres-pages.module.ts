import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultationOeuvresComponent } from './consultation-oeuvres/consultation-oeuvres.component';
import { OeuvresViewsModule } from 'src/views/oeuvres/oeuvres-views.module';
import { OeuvresRoutingModule } from './oeuvres-routing.module';
import { OeuvreServicesModule } from 'src/services/oeuvre/oeuvre-services.module';



@NgModule({
  declarations: [ ConsultationOeuvresComponent ],
  exports: [ ],
  providers: [
  ],
  imports: [
    CommonModule,
    OeuvresViewsModule,
    OeuvresRoutingModule,
    OeuvreServicesModule
  ]
})
export class OeuvresPagesModule { }
